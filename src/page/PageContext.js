import React from "react";
import { Add } from "../component/Add";
import { List } from "./List";

export const PageContext = () => {
  return (
    <div>
      <List />
      <Add />
    </div>
  );
};
