import React from 'react'
import { Log } from './Log'
import { Logout } from './Logout'

export const InLog = () => {
  return (
    <div>
        <Log/>
        <Logout/>
    </div>
  )
}
