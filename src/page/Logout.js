import React from "react";
import { useAuthContext } from "../Context/AuthProvider ";

export const Logout = () => {
  const { isAuthenticated, logout } = useAuthContext();

  const handleLogout = () => {
    logout();
  };

  return (
    <div>
      <h1>Home Page</h1>
      {isAuthenticated && <button onClick={handleLogout}>Logout</button>}
    </div>
  );
};
