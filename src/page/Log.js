import React from "react";
import { useAuthContext } from "../Context/AuthProvider ";

export const Log = () => {
  const { login } = useAuthContext();

  const handleLogin = () => {
    login();
  };

  return (
    <div>
      <h1>Login Page</h1>
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};
