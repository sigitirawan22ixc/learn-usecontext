import React, { createContext, useContext, useReducer } from "react";

// inisiai bank dat teller
const StorageData = createContext(null);
const ToolsData = createContext(null);

export const CommonContext = ({ children }) => {
  // proses pembuatan Alat Transaksi  ==================================

  // data awal alat
  const initialTasks = [
    { id: 0, title: "berani", color: "red" },
    { id: 1, title: "Suci", color: "white" },
    { id: 2, title: "sigma", color: "black" },
  ];

  // alat yang di gunakan untuk transaksi

  // fungsi fungsi yang ada dalam alat
  const tasksReducer = (tasks, action) => {
    // action = di gunakan untuk menentukan aksi apa yang kita lakukan
    console.log(tasks, "reducer");
    switch (action.type) {
      case "adds": {
        return [
          ...tasks,
          {
            id: action.id,
            title: action.title,
            color: action.color,
          },
        ];
      }
      default: {
        throw Error("Unknown action: " + action.type);
      }
    }
  };

  const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);

  // End proses pembuatan Alat Transaksi  ==================================

  return (
    <StorageData.Provider value={tasks}>
      <ToolsData.Provider value={dispatch}>{children}</ToolsData.Provider>
    </StorageData.Provider>
  );
};

// untuk  mengambil data
export const useGetData = () => {
  return useContext(StorageData);
};

export const useFungsiDispatch = () => {
  return useContext(ToolsData);
};
