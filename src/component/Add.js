import React, { useState } from "react";
import { useFungsiDispatch } from "../Context/CommonContext";
export const Add = () => {
  const [addTitle, setAddTitle] = useState("");
  const [addColor, setAdColor] = useState("");
  const dispatch = useFungsiDispatch()
  let nextId = 3;
  return (
    <div>
      <h6>Title :</h6>
      <input
        onChange={(e) => {
          setAddTitle(e.target.value);
        }}
      />
      <h6>Color :</h6>
      <input
        onChange={(e) => {
          setAdColor(e.target.value);
        }}
      />
      <button
        onClick={() => {
          dispatch({
            type: "adds",
            id: nextId++,
            title: addTitle,
            color: addColor,
          });
          setAddTitle("");
          setAdColor("");
        }}
      >Add</button>
    </div>
  );
};
