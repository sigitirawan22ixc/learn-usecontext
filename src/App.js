// import { CommonContext } from "./Context/CommonContext";
// import { List } from "./page/List";

import { InLog } from "./page/InLog";
import { PageContext } from "./page/PageContext";

function App() {
  return (
    <div>
      <PageContext />
      <InLog />
    </div>
  );
}

export default App;
